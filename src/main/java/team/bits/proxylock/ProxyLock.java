package team.bits.proxylock;

import com.destroystokyo.paper.event.server.PaperServerListPingEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class ProxyLock extends JavaPlugin implements Listener {

    private static final String BUNGEE_HOSTNAME = "play.bits.team";
    private static final String KICK_MESSAGE = ChatColor.DARK_RED + "Unauthorized";
    private static final String ERROR_MESSAGE = "An unknown error has occurred";

    private InetAddress bungeeAddress;

    @Override
    public void onEnable() {
        try {
            try {
                this.bungeeAddress = InetAddress.getByName(BUNGEE_HOSTNAME);
            } catch (UnknownHostException ex) {
                this.getLogger().severe(String.format("Cannot resolve hostname for '%s'", BUNGEE_HOSTNAME));
                Bukkit.getPluginManager().disablePlugin(this);
                return;
            }

            Bukkit.getPluginManager().registerEvents(this, this);

            this.getLogger().info(String.format(
                    "ProxyLock setup, only accepting connections from %s (%s)",
                    BUNGEE_HOSTNAME, this.bungeeAddress.getHostAddress()
            ));
        } catch (final Exception ex) {
            this.getLogger().severe("Exception occurred during startup");
            ex.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll((JavaPlugin) this);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onPlayerLogin(@NotNull PlayerLoginEvent event) {
        try {
            if (isAddressNotWhitelisted(event.getRealAddress())) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, KICK_MESSAGE);

                this.getLogger().info(String.format(
                        "%s tried to join from %s and was denied access",
                        event.getPlayer().getName(), event.getRealAddress().getHostAddress()
                ));
            }
        } catch (final Exception ex) {
            this.getLogger().severe("Exception occurred during player login");
            ex.printStackTrace();

            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, ERROR_MESSAGE);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onPaperServerListPing(@NotNull PaperServerListPingEvent event) {
        try {
            if (isAddressNotWhitelisted(event.getAddress())) {
                event.setCancelled(true);
            }
        } catch (final Exception ex) {
            this.getLogger().severe("Exception occurred during server list ping");
            ex.printStackTrace();
        }
    }

    private boolean isAddressNotWhitelisted(@NotNull InetAddress address) {
        return !address.getHostAddress().equalsIgnoreCase(this.bungeeAddress.getHostAddress());
    }
}
